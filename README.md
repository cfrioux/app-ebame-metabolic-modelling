# App Metabolic network reconstruction and modelling

Integration of tools for metabolic network reconstruction and metabolic modelling for the EBAME-2023 school. 

This app repository is designed for [IFB Biosphere deployment](https://biosphere.france-bioinformatique.fr/catalogue/appliance/223/).
It was designed (and debugged) with the help of Christophe Blanchet (IFB) (his [repo](https://gitlab.in2p3.fr/christophe.blanchet/app-metabolic-modelling/-/tree/main?ref_type=heads)).

It is associated to a [tutorial](https://gitlab.inria.fr/cfrioux/ebame).

This repository content can be used to deploy a VM without Biosphere provisioning, use only the file `pythonapp-install.sh` and uncomment the first lines dedicated to conda and mamba installs. 


## Description

**[8th EBAME Workshop on Computational Microbial Ecogenomics](https://maignienlab.gitlab.io/ebame)**

Microbial Ecology is undergoing rapid evolution and dramatic progresses thanks to the combined advances of next-generation DNA sequencing technologies and computational approaches for data analysis and visualization. As a result,"microbial ecogenomics" has become an essential part of investigations of marine or terrestrial habitats, or host-microbe interactions. The aim of the EBAME workshop is to bring together researchers who generate complex 'omics datasets to investigate biologically ecological and evolutionnary questions with researchers who develop new concepts and computational methods to analyze such datasets.

**Tutorial on metabolic network reconstruction and modelling**

For this session we will learn to reconstruct, explore and simulation metabolic networks at the scales of the species and of the community.

**Guide**

 Once the VM is up, you can connect with SSH, and follow the [tutorial](https://gitlab.inria.fr/cfrioux/ebame)

```
ssh ubuntu@<your.vm.ip.address>
```

## Tools

* Tools : Ubuntu 22.04 
* [gapseq](https://github.com/jotech/gapseq)
* [padmet](https://github.com/AuReMe/padmet)
* [prodigal](https://github.com/hyattpd/Prodigal)
* [kegg2bipartitegraph](https://github.com/ArnaudBelcour/kegg2bipartitegraph)
* [MeneTools](https://github.com/cfrioux/MeneTools)
* [MiSCoTo](https://github.com/cfrioux/miscoto)
* [Metage2Metabo](https://github.com/AuReMe/metage2metabo)

## Topics

* Microbial Ecology
* Metabolic Modelling
* Metabolic Networks
* Metagenomics

## Contact

* Clémence Frioux (Inria)

## Developers

* [Clémence Frioux](https://cfrioux.github.io/) (Inria)
* Christophe Blanchet ([IFB-core](https://www.france-bioinformatique.fr/organisation-gouvernance/cellules-operationnelles/ifb-core-cellule/))
