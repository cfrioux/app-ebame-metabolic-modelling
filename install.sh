#!/bin/bash

# This script is executed on the virtual machine during the Installation phase (need to be ran as root!).
# It is used to record a predefined VM-image of the appliance.
# Otherwise executed first during a cloud deployement in IFB-Biosphere

# Installation of python/pip/conda apps raises errors with cloud-installer client
# Install through a separated ssh connection to localhost

# Authorize local SSH key for current user
USER_KEY_FILE="${HOME}/.ssh/biosphere_id_rsa"
USER_PUBKEY_FILE="${USER_KEY_FILE}.pub"
echo "Set Biosphere local RSA key..."
if [[ ! -e ${USER_PUBKEY_FILE} ]]
then
    echo "No RSA key found."
    ssh-keygen -t rsa -N '' -q -f ${USER_KEY_FILE}
else
    echo "RSA key found."
fi
ansible-playbook -b -e 'ansible_python_interpreter=/usr/bin/python3' \
    -e "user_name=${USER}" \
    -e "user_pubkey_file=${USER_PUBKEY_FILE}" \
    ssh-pubkey-config.yaml

# Install python envs
ansible-playbook -i 127.0.0.1, -b --private-key ${USER_KEY_FILE} \
    -e 'ansible_python_interpreter=/usr/bin/python3' \
    -e "ansible_ssh_common_args='-o StrictHostKeyChecking=no'" \
    pythonapp-play.yaml
