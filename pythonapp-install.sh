#!/bin/bash

# This script is executed on the virtual machine during the Installation phase (need to be ran as root!).
# It is used to record a predefined VM-image of the appliance.
# Otherwise executed first during a cloud deployement in IFB-Biosphere

# Uncomment the following line to enable deploying a VM from a predefined Ubuntu image and not through IFB Biosphere
# Install conda & mamba
# wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O /tmp/miniconda.sh
# bash /tmp/miniconda.sh -b -f -p $HOME/miniconda
# echo ". $HOME/miniconda/etc/profile.d/conda.sh" >> ~/.bashrc
# eval "$(~/miniconda/bin/conda shell.bash hook)"
# conda install -n base --override-channels -c conda-forge mamba 'python_abi=*=*cp*' -y

source /etc/profile
APP_CONFIG_URL1=https://gitlab.inria.fr/cfrioux/ebame/-/raw/main/conda/ebame_metabo_gapseq.yml?ref_type=heads
APP_CONFIG_URL2=https://gitlab.inria.fr/cfrioux/ebame/-/raw/main/conda/ebame_metabo_reasoning.yml?ref_type=heads
APP_DIR=/ifb/arch/metabo

APP_CONFIG_FILE1=$APP_DIR/ebame_metabo_gapseq.yml
APP_CONFIG_FILE2=$APP_DIR/ebame_metabo_reasoning.yml

echo "DEPLOYMENT_LOG --- Creating app directory..."

sudo mkdir -p $APP_DIR
sudo chown -R $USER $APP_DIR

echo "DEPLOYMENT_LOG --- Downloading conda env recipes..."
curl -LJ "$APP_CONFIG_URL1" -o $APP_CONFIG_FILE1
curl -LJ "$APP_CONFIG_URL2" -o $APP_CONFIG_FILE2

echo "DEPLOYMENT_LOG --- App file: $APP_CONFIG_FILE1"
head $APP_CONFIG_FILE1
echo "DEPLOYMENT_LOG --- App file: $APP_CONFIG_FILE2"
head $APP_CONFIG_FILE2

# create conda envs
echo "DEPLOYMENT_LOG --- Creating conda envs..."
echo "DEPLOYMENT_LOG --- Creating gapseq env..."
mamba env create -f $APP_CONFIG_FILE1
sleep 2m
echo "DEPLOYMENT_LOG --- creating reasoning env..."
mamba env create -f $APP_CONFIG_FILE2
# add an env for https://forgemia.inra.fr/artemis/artemis-workshop tutorial
echo "DEPLOYMENT_LOG --- creating numerical_modelling env..."
mamba create -n numerical_modelling jupyter -y
mamba activate numerical_modelling
pip install SALib scikit-learn plotly numpy pandas scipy openpyxl
mamba deactivate

# update NCBI taxonomy in ete3 package
echo "DEPLOYMENT_LOG --- Updating NCBI taxonomy in ete3 package for ebame_metabo_reasoning env..."
conda activate ebame_metabo_reasoning
echo -e "from ete3 import NCBITaxa\nncbi = NCBITaxa()\nncbi.update_taxonomy_database()" | python
# install the latest version of Menetools and Kegg2Bipartite
echo "DEPLOYMENT_LOG --- Installing Kegg2Bipartite..."
pip install git+https://github.com/ArnaudBelcour/kegg2bipartitegraph.git
pip install memote
pip install git+https://github.com/bioasp/seed2lp
conda deactivate

# clone gapseq and update its databases
echo "DEPLOYMENT_LOG --- Cloning gapseq and updating its databases..."
mkdir -p ~/ebame_tutorial
cd ~/ebame_tutorial
conda activate ebame_metabo_gapseq
# clone gapseq repository
git clone https://github.com/jotech/gapseq.git
# install missing R package
echo "DEPLOYMENT_LOG --- Installing missing R packages for gapseq..."
R -e 'install.packages("CHNOSZ", repos="http://cran.us.r-project.org")'
R -e 'install.packages("httr", repos="http://cran.us.r-project.org")'
R -e 'BiocManager::install("IRanges")'
# Download & Install R-package 'sybilSBML'
wget https://cran.r-project.org/src/contrib/Archive/sybilSBML/sybilSBML_3.1.2.tar.gz
R CMD INSTALL --configure-args=" \
--with-sbml-include=$CONDA_PREFIX/include \
--with-sbml-lib=$CONDA_PREFIX/lib" sybilSBML_3.1.2.tar.gz
rm sybilSBML_3.1.2.tar.gz
# Download reference sequence data
bash ./gapseq/src/update_sequences.sh

# clone the gitlab project of the tutorial
echo "DEPLOYMENT_LOG --- Cloning the gitlab project of the tutorial..."
git clone https://gitlab.inria.fr/cfrioux/ebame.git

echo "DEPLOYMENT_LOG --- Downloading the jar file dependency necessary for m2m_analysis..."
# download the jar file dependency necessary for m2m_analysis
wget https://github.com/AuReMe/metage2metabo/raw/main/external_dependencies/Oog_CommandLineTool2012/Oog.jar